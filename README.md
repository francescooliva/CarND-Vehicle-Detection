**Vehicle Detection Project**

The goals / steps of this project are the following:

* Perform a Histogram of Oriented Gradients (HOG) feature extraction on a labeled training set of images and train a classifier Linear SVM classifier
* Implement a sliding-window technique and use your trained classifier to search for vehicles in images.
* Run pipeline on a video stream and create a heat map of recurring detections frame by frame to reject outliers and follow detected vehicles.
* Estimate a bounding box for vehicles detected.

[//]: # (Image References)
[image1]: ./output_images/car_not_car.png
[image2]: ./output_images/HOG_example.png
[image3]: ./output_images/sliding_windows1.png
[image4]: ./output_images/sliding_windows_mutiscale.png
[image5]: ./output_images/heatmaps.png
[image6]: ./output_images/pipeline_test.png
[video1]: ./output_videos/project_video_processed.mp4

---

### Histogram of Oriented Gradients (HOG)

#### 1. HOG features extration from the training images.

The code for this step is contained in the code cell [3] of the IPython notebook `.project/vehicle-detection.ipynb`.

I started with data exploration by reading in all the `vehicle` and `non-vehicle` images into two car/not-car filenames lists, with a total of  8792 cars samples and 8968 non-cars. Here is an example of one of each of the `vehicle` and `non-vehicle` classes:

![alt text][image1]

I then grabbed random images from each of the two classes and I then explored  different `skimage.hog()` parameters (`orientations`, `pixels_per_cell`, and `cells_per_block`) to feed the method `get_hog_features` in the code cell [4] of the notebook.

Here is an example using HOG parameters of `orientations=9`, `pixels_per_cell=(8, 8)` and `cells_per_block=(2, 2)`:


![alt text][image2]

#### 2. Settling on final choice of HOG parameters.

I tried various combinations of HOG parameters, color spaces and spatial binnings and used them to train a linear SVM  and chose a compromise between performance and speed, my final configuration was based on accuracy of my classifier and the final performance on the test images and I ended up using 9 orientations 8 pixels per cell and 2 cells per block

#### 3.Training a classifier using your selected HOG features.

I trained a linear SVM with the default classifier parameters, using all channels of images converted to YCrCb space (code cell [6]). I included spatial features and color features and the final feature vector has a length of 8460 elements, 5292 of theme were HOG features. After training this resulted in a test accuracy of 100%.

### Sliding Window Search

#### 1. Implementing a sliding window search.  How did you decide what scales to search and how much to overlap windows?

In code cell [9] and [10] to  implement the sliding window search I used and adapted the method find_cars from the lesson materials, defining for each window a scaling factor of 1.0

![alt text][image3]

Also I ran this same function multiple times for different scale values (1x, 1.5x, 2x) to generate multiple-scaled search windows.

![alt text][image4]


#### 2. Implementing filter for false positives and some method for combining overlapping bounding boxes.

In code cell [9] from some test images I created a heatmap and then thresholded that map to identify vehicle positions.  I then used `scipy.ndimage.measurements.label()` to identify individual blobs in the heatmap.  I then assumed each blob corresponded to a vehicle.  I constructed bounding boxes to cover the area of each blob detected.  

Here's an example result showing the heatmap from a series of images, the result of `scipy.ndimage.measurements.label()` and the bounding boxes then overlaid on the last frame of video:

Here is the output of `scipy.ndimage.measurements.label()` and their corresponding heatmaps from the six images processed above and the resulting bounding boxes:

![alt text][image5]


#### 3. Show some examples of test images to demonstrate how your pipeline is working.  What did you do to optimize the performance of your classifier?

In code cell [11] using YCrCb 3-channel HOG features with spatially binned color and histograms of color in the feature vector, which provided a nice result.  Here are some example images:


![alt text][image6]

 After analyzing all the frames i the video I calculated the height and width of my bounding boxes and only drew them if they were > 400 pixels and > 600 respectively.
 After exploring the C parameter of the linear SVM  I reduced my false classifications by putting C = 5.
 I used a thresholding the decision function `svc.decision_function(sample) > 0.60`  to improve high confidence predictions and reduce false positives
 Then for video implementation I used the python container collections.deque to keep track of heatmaps of the 10 last frames and  average them.

---

### Video Implementation

#### 1. Provide a link to your final video output.  Your pipeline should perform reasonably well on the entire project video (somewhat wobbly or unstable bounding boxes are ok as long as you are identifying the vehicles most of the time with minimal false positives.)
Here's a [link to my video result](./output_videos/project_video_processed.mp4)


---

### Discussion

#### 1. Briefly discuss any problems / issues you faced in your implementation of this project.

####  Where will your pipeline likely fail?
I got false positives still after heatmap filtering, expecially with oncoming cars, distant cars, guardrail and in some test images traffic signals. So the pipeline is probably going to fail in cases where vehicles  don't resemble those in the training set.

####  What could you do to make it more robust?
A database with more image samples for sure.
It could be an improvement to parallelize the code (gpu computing or multicore) for a multiscale parallel searching of sliding windows.
